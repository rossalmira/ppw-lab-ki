//to intialize and load a js sdk asynchronous
// FB initiation function
  window.fbAsyncInit = () => {
    FB.init({
      appId      : '509560642756732',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.11'
    });
	
  // implement a function that check login status (getLoginStatus)
  // and run render function below, with a boolean true as a paramater if
  // login status has been connected
    FB.getLoginStatus(function(response) {
      console.log("status : "+ response['status']);
      render(response['status']==='connected');
    });
  };

  // This is done because when the user opened the web and the user has been logged in,
  // it will automatically display the logged in view
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

   
// Render function, receive a loginFlag paramater that decide whether
// render or create a html view for the logged in user or not
// Modify this method as needed if you feel you need to change the style using
// Bootstrap's classes or your own implemented CSS

  const render = loginFlag => {
    if (loginFlag) {

  getUserData(user => {
        // Render profile view, post input form, post status button, and logout button
        $('#lab8').html(
          '<div class="profile">' +
            '<img class="cover" src="' + user.cover.source + '" alt="cover" />' +
            '<img class="picture" src="' + user.picture.data.url + '" alt="profpic" />' +
            '<div class="data">' +
              '<h1>' + user.name + '</h1>' +
              '<h2>' + user.about + '</h2>' +
              '<h3>' + user.email + ' - ' + user.gender + '</h3>' +
            '</div>' +
          '</div>' +
          '<input id="postInput" type="text" class="post" placeholder="Write your status" />' +
          '<button class="postStatus" onclick="postStatus()">Post</button>' +
          '<button class="logout" onclick="facebookLogout()">Logout</button>'
        );

 // After renderin the above view, get home feed data from the logged in account
      // by calling getUserFeed method which you implement yourself.
      // That method has to receive a callback parameter, which receive a feed object as a response
      // from calling the Facebook API


        getUserFeed(feed => {
            feed.data.map(value => {
               
                let id  = "'" + value.id + "'";
                if (value.message && value.story) {
                    $('#feeds').append(
                        '<div class="feed">' +
                        '<h1>' + value.message + '</h1>' +
                        '<h2>' + value.story + '</h2>' +
                        '<button class="deletePost" onclick="deleteFeed(' + id + ')">Delete</button>' +
                        '</div>'
                    );
                } else if (value.message) {
                    $('#feeds').append(
                        '<div class="feed">' +
                        '<h1>' + value.message + '</h1>' +
                        '<button class="deletePost" onclick="deleteFeed(' + id + ')">Delete</button>' +
                        '</div>'
                    );
                } else if (value.story) {
                    $('#feeds').append(
                        '<div class="feed">' +
                        '<h2>' + value.story + '</h2>' +
                        '<button class="deletePost" onclick="deleteFeed(' + id + ')">Delete</button>' +
                        '</div>'
                    );
                }
            });
        });
      });
    } else {
   
      $('#lab8').html('<button class="login" onclick="facebookLogin()">Login</button>');
    }
  };

  const facebookLogin = () => {
      FB.login(response => {
          render(true);
      }, {scope:'public_profile,email,user_about_me,user_posts,publish_actions'});
  };

  const facebookLogout = () => {
    FB.logout(response => {
        render(false);
    });
  };


  const getUserData = (fun) => {
    FB.api('/me?fields=cover,picture,name,about,email,gender', 'GET', function (response){
      fun(response);
    });
  };

  const getUserFeed = (fun) => {
 
    FB.api(
        "/me/feed",
        function (response) {
          if (response && !response.error) {
            fun(response);
          }
        }
    );
  };

  const postFeed = (message) => {
    FB.api(
        "/me/feed",
        "POST",
        {
            "message": message
        },
        function (response) {
          console.log(response);
          if (response && !response.error) {
            location.reload();
          }
        }
    );
  };

  const deleteFeed = (id) => {
    FB.api(
    id,
    "DELETE",
    function (response) {
      if (response && !response.error) {
        location.reload();
      }
    }
);
  }

  const postStatus = () => {
    const message = $('#postInput').val();
    postFeed(message);
  };

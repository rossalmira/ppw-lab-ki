//to intialize and load a js sdk asynchronous

window.fbAsyncInit = function() {
    FB.init({
      appId      : '509560642756732',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.11'
    });
  // implement a function that check login status (getLoginStatus)
  // and run render function below, with a boolean true as a paramater if
  // login status has been connected
FB.getLoginStatus(function(response) {
  console.log("status : "+ response['status']);
  render(response['status']==='connected');
});
};


  // This is done because when the user opened the web and the user has been logged in,
  // it will automatically display the logged in view


// Facebook call init. default from facebook
(function(d, s, id){
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) {return;}
  js = d.createElement(s); js.id = id;
  js.src = "https://connect.facebook.net/en_US/sdk.js";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// Render function, receive a loginFlag paramater that decide whether
// render or create a html view for the logged in user or not
// Modify this method as needed if you feel you need to change the style using
// Bootstrap's classes or your own implemented CSS

const render = loginFlag => {
  if (loginFlag) {
    // If the logged in view the one that will be rendered

    // Call getUserData method (see below) that have been implemented by you with a callback function
    // that receive user object as a parameter.
    // This user object is the response from an API facebook call.
    getUserData(user => {
      // Render profile view, post input form, post status button, and logout button
      $('#lab8').html(
        '<div class="profile">' +
        '<img class="cover" src="' + user.cover.source + '" alt="cover" />' +
        '<img class="picture" src="' + user.picture.data.url + '" alt="profpic" />' +
        '<div class="data">' +
        '<h1>' + user.name + '</h1>' +
        '<h2>' + user.about + '</h2>' +
        '<h3>' + user.email + ' - ' + user.gender + ' - ' + user.status + '</h3>' +
        '</div>' +
        '</div>' +
        '<input id="postInput" type="text" class="post" placeholder="Ketik Status Anda" />' +
        '<button class="postStatus" onclick="postStatus()">Post ke Facebook</button>' +
        '<button class="logout" onclick="facebookLogout()">Logout</button>' +
        '<br>' +
        '<br>' +
        '<br>' +
        '<h1> Feed </h1>'
      );

      // After renderin the above view, get home feed data from the logged in account
      // by calling getUserFeed method which you implement yourself.
      // That method has to receive a callback parameter, which receive a feed object as a response
      // from calling the Facebook API
	  
	  
      getUserFeed(feed => {
        //alert("test")
        feed.data.map(value => {
		 
          // Render the feed, customize as needed.
          if (value.message && value.story) {
            $('#lab8').append(
              '<div class="feed">' +
              '<h1>' + value.message + '</h1>' +
              '<h2>' + value.story + '</h2>' +
			  '<button class="deletePost" onclick="deleteFeed(' + id + ')">Delete</button>' +
              '</div>'
            );
          } else if (value.message) {
            $('#lab8').append(
              '<div class="feed">' +
              '<h1>' + value.message + '</h1>' +
              '<button class="deletePost" onclick="deleteFeed(' + id + ')">Delete</button>' +
			  '</div>'
            );
          } else if (value.story) {
            $('#lab8').append(
              '<div class="feed">' +
              '<h2>' + value.story + '</h2>' +
              '<button class="deletePost" onclick="deleteFeed(' + id + ')">Delete</button>' +
			  '</div>'
            );
          }
        });
      });
    });
  } else {
    // The view when not logged in yet
    $('#lab8').html('<button class="login" onclick="facebookLogin()">Login</button>');
  }
};


 // TODO: Implement this method
  // Make sure this method receive a callback function that will call the render function
  // that will render logged in view after successfully logged in,
  // and this function has all needed permissions at the above scope.
  // You can modify facebookLogin function above
const facebookLogin = () => {
 
	
		FB.login(response => {
          render(true);

	},    
	{scope:'email, public_profile, user_posts, user_about_me, publish_actions, publish_pages'})
};

const facebookLogout = () => {
  // TODO: Implement this method
  // Make sure this method receive a callback function that will call the render function
  // that will render not logged in view after successfully logged out.
  // You can modify facebookLogout function above
   FB.logout(response => {
        render(false);
    });
  };

  
// TODO: Complete this method
// This method modify the above getUserData method that receive a callback function called fun
// and the request user data from the logged in account with all the needed fields in render method,
// and call that callback function after doing the request and forward the response to that callback function.
// What does it mean by a callback function?


  const getUserData = (fun) => {
    FB.api('/me?fields=cover,picture,name,about,email,gender', 'GET', function (response){
      fun(response);
    });
  };



  // TODO: Implement this method
  // Make sure this method receive a callback function parameter and do a data request to Home Feed from
  // the logged in account with all the needed fields in render method, and call that callback function
  // after doing the request and forward the response to that callback function.
 
const getUserFeed = (fun) => { 
	FB.api(
        "/me/feed",
        function (response) {
          if (response && !response.error) {
            fun(response);
          }
        }
    );
  };
const deleteFeed = (id) => {
    FB.api(
    id,
    "DELETE",
    function (response) {
      if (response && !response.error) {
        location.reload();
      }
    }
);
  }



const postFeed = (message) => {
  // Todo: Implement this method,
  // Make sure this method receive a string message parameter and do a POST request to Feed
  // by going through Facebook API with a string message parameter as a message.
  FB.api('/me/feed', 'POST', {message:message});
  render(true);
};



//add alert
const postStatus = () => {
  const message = $('#postInput').val();
  if(message == ""){
    alert("You haven't write anything")
  } else{
    postFeed(message);
  }
};

from django.shortcuts import render
from datetime import datetime, date

mhs_name = 'Rossalmira'
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1996, 8, 13) 

def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year)}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0

# Lab 7 : _Web Service_ Introduction

CSGE602022 - Web Design & Programming (Perancangan & Pemrograman Web) @
Faculty of Computer Science Universitas Indonesia, Odd Semester 2017/2018

* * *

## Objectives

After finishing this tutorial, students are expected to understand:
- What is Webservice, ajax and json
- Able to use Webservice, ajax and Json

## End Result
- Create a page consist of Fasilkom UI students list using api from api-dev.cs.ui.ac.id


## Checklist

### Mandatory
1. I've created a page to show all fasilkom students
    1. [ ] I've created a list consist of fasilkom students, that can be called from django model.
    2. [ ] I've created a button to add a list of students into the friend list (implemented using ajax).
    3. [ ] I implemented validate_npm to check whether the friend i wanted to add is already on the friend list or not.
    4. [ ] I've created pagination (hint: one of the response data from api.cs.ui.ac.id is `next` and `previous` that is used to create pagination)
2. I've created a page to show friend list 
    1. [ ] I've created a list that consist of friend list, the list can be implemented using ajax.
    2. [ ] I've created a button to delete friend from friend list (implemented using ajax).
3. I have good _Code Coverage_ 
    1. [ ] if you haven't configure it to show _Code Coverage_ on Gitlab, then look at the steps on `Show Code Coverage in Gitlab` in [README.md](https://gitlab.com/PPW-2017/ppw-lab/blob/master/README.md)
    2. [ ] Make sure your _Code Coverage_ is 100%


### Additional

1. Created a page to show friend's full data
    1. [ ] The page can be opened everytime the user click one of the friends on friend list
    1. [ ] Add google maps to show the address of our friend on detail information page (hint: https://developers.google.com/maps/documentation/javascript/)
1. ".env" file are used to store username and password, which can cause your account to be opened by other people who has access to your repository if that file is pushed to the repo.
   Therefore it is not a good practice and can be embarassing because you show your own secret/privacy.
    1. [ ] Protect your secret and privacy. Change the storing mechanism if needed.
   

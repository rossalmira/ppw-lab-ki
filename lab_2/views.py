from django.shortcuts import render
from lab_1.views import mhs_name, birth_date

#TODO Implement
#Create a content paragraph for your landing page:
landing_page_content = 'My name Rossalmira. I am 21 years old.  I love to travel because it gives a sense of adventure and freedom. It allows us to get away from “normal” life, experience other cultures, to get to know places we have only seen in movies. We get to try new food and meet new people.'

def index(request):
    response = {'name': mhs_name, 'content': landing_page_content}
    return render(request, 'base2.html', response)